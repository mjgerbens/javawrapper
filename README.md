Wrapper made for RandomForest model project.
Project name: Classifying Epileptic Brain Activity:A Machine Learning Research

This program was made as a part of a bigger research[1] The wrapper was made for classifying 
epileptic activity in the brain. The wrapper makes use of the classifying model RandomForest. 
Because the program runs RandomForest it can also be used to classify other datasets.

Needed Software:
-- IntelliJ IDEA (Ultimate not needed)
-- JDK 10.0.2
-- SDK (Java 10.0.2)
-- Gradle

Usage:

1. Download this repository(clone)
2. Open the downloaded project in IntelliJ
3. Edit run configuration of ArgparserRunner.java(topright)
4. Run the program with the following Program arguments(configuration):

-f, --FILE <path> Exact path to the .arff file which already has classified classes. 

        Example: testdata/algodata.arff 

-u, --UNKNOWNFILE <path> Exact path to the unclassified dataset. 

        Example: testdata/unknownFile.arff 
        
-h, --HELP  Prints a help message. 

5: Analyze the output in the console 

Output:

-- First the unknown instances will be printed into the console.(unknown file data)

-- Below the unknown instances will be the instances with the classified classes.



Author: Menno Gerbens

Acknowledgements:

-- Michiel Noback for providing base wrapper

Sources:

[1] Full research: https://bitbucket.org/mjgerbens/thema09projectmjg/src/master/
