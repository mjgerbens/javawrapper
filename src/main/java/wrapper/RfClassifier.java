package wrapper;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class RfClassifier {

    private final String modelFile = "testdata/RandomForestmodel.model";
    private final OptionsProvider optionsProvider;

    public RfClassifier( OptionsProvider optionsPovider) {

        this.optionsProvider = optionsPovider;
    }


    public void start(){
        String datafile = optionsProvider.getFile();
        String unknownFile = optionsProvider.getUnFile();
        try {
            Instances instances = loadArff(datafile);
            printInstances(instances);
            RandomForest rf = buildClassifier(instances);
            saveClassifier(rf);
            RandomForest fromFile = loadClassifier();
            Instances unknownInstances = loadArff(unknownFile);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(RandomForest rf, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = rf.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }
    private RandomForest loadClassifier() throws Exception {
        // deserialize model
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);

    }

    private void saveClassifier(RandomForest rf) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, rf);

        // serialize model pre 3.5.5
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFile));
//        oos.writeObject(j48);
//        oos.flush();
//        oos.close();
    }
    private RandomForest buildClassifier(Instances instances) throws Exception {
        FileInputStream changedModelFile = new FileInputStream("testdata/RandomForestmodel.model");
        RandomForest rf = (RandomForest) (new ObjectInputStream(changedModelFile)).readObject();
        rf.setNumIterations(400);
        rf.buildClassifier(instances);   // build classifier
        return rf;
    }
    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }
    }



    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}

