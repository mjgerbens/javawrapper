package wrapper;


import org.apache.commons.cli.*;


public class Argparser implements OptionsProvider {
    private static final String FILE = "file";
    private static final String HELP = "help";
    private static final String UNKNOWNFILE = "unknown file";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    public Argparser(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    private void initialize() {
        buildOptions();
        processCommandline();

    }
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);}

    private void buildOptions() {
        this.options = new Options();
        Option fileOption = new Option("f", FILE, true, "File used for classifying");
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option ufileOption = new Option("u", UNKNOWNFILE, true, "File that needs to be classified");

        options.addOption(fileOption);
        options.addOption(helpOption);
        options.addOption(ufileOption);

    }

    private void processCommandline() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }

    }
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyCoolTool", options);
    }

    @Override
    public String getFile() {
        return this.commandLine.getOptionValue(FILE);
    }

    @Override
    public String getUnFile() {
        return this.commandLine.getOptionValue(UNKNOWNFILE);
    }
}
