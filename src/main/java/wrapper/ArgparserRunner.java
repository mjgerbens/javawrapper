package wrapper;

import java.util.Arrays;

public class ArgparserRunner {

    private ArgparserRunner(){

    }

    public static void main(String[] args) {
        try {
            Argparser op = new Argparser(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }
            RfClassifier controller = new RfClassifier(op);
            controller.start();
        }
         catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            Argparser op = new Argparser(new String[]{});
             op.printHelp();
        }

    }
}
